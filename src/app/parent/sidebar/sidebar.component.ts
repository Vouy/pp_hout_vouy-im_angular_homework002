import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
    option : any = "";
    // @Input() subjectName : string = "";
    // @Input() photo : string = "";

    @Input() subject_list : any;

    @Output() subjectClicked = new EventEmitter<string>();
    chooseSub(value : any) : any {
      this.option = value;
      this.subjectClicked.emit(this.option);
      return this.option;
    }
}
