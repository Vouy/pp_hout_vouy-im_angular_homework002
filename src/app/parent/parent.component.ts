import { Component, Input , Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent {
  subject_list :  Array<any> = [ {
    subjectName: 'Angular',
    description: `Angular is an open framework and
    platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
    thumbnail: 'angular.svg', },
    {
    subjectName: 'TypeScript',
    description: `TypeScript is a strongly typed, object oriented, compiled
    language.
    TypeScript is a syntactic superset of JavaScript which adds static typing. It was designed by Anders Hejlsberg (designer of C#) at Microsoft.`,
    thumbnail: 'typescript.svg',
    }, {
    subjectName: 'Kotlin',
    description: `Kotlin is a modern, trending programming language. Kotlin is easy to learn,
    especially if you already know Java (it is 100% compatible with Java).`,
    thumbnail: 'kotlin.svg', },
    {
    subjectName: 'Java',
    description: `Java is a high-level, class-based, object-oriented
    programming language that is designed to
    have as few implementation dependencies as possible.`,
    thumbnail: 'java.svg', },
    {
    subjectName: 'JavaScript',
    description: `JavaScript often abbreviated JS, is a programming language
    that is one of the core technologies
    of the World Wide Web, alongside HTML and CSS.`,
    thumbnail: 'javascript.svg', },
    ]

    // sub_FormChild : string = "";
  
    // getSubjectFormChild (value : string) : string {
    //     this.sub_FormChild = value;
    //     return this.sub_FormChild;
    // }

    Default: Array<any> = [{
      subjectName: 'Angular',
      description: `Angular is an open framework and
    platform for creating Single Page Applications, written in TypeScript and supported and developed by Google.`,
      thumbnail: 'angular.svg',
    }]

    @Output() clickLogin = new EventEmitter<void>()

    onLogin(){
      this.clickLogin.emit()
    }

    getClickSubject(subjectName: string){
      this.Default = this.subject_list.filter((res)=> res.subjectName === subjectName)
    }

}
