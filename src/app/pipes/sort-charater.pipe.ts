import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortCharater'
})
export class SortCharaterPipe implements PipeTransform {

  transform(student : any[] ) : any {
    return student.sort((x,y) => x.subjectName.localeCompare(y.subjectName));
  }

}
