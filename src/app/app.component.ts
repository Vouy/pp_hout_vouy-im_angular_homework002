import { Component , Input, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'HOMEWORK001';
  buttonLogin : boolean = false;

   clickLogin() {
    if(this.buttonLogin == false){
      this.buttonLogin = true;
    }else{
      this.buttonLogin = false;
    }    
  }

}
