import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ParentComponent } from './parent/parent.component';
import { SidebarComponent } from './parent/sidebar/sidebar.component';
import { ContentComponent } from './parent/content/content.component';
import { LoginLogoutDirective } from './directives/login-logout.directive';
import { SortCharaterPipe } from './pipes/sort-charater.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    SidebarComponent,
    ContentComponent,
    LoginLogoutDirective,
    SortCharaterPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
