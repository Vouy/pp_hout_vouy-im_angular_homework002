import { Directive , Input , TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appLoginLogout]'
})
export class LoginLogoutDirective {
  private hasView = false;

  constructor(
   private templateRef : TemplateRef<any>,
   private viewContainer: ViewContainerRef) { }
  
  @Input() set appLoginLogout(condition: boolean) {
  
  /** Add the template content to the DOM unless the condition is true. */
  if (!condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
   } 
  else if (condition && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
   }
  }
}
